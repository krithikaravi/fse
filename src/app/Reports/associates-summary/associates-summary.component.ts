import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { AssociateDetails } from '../../classes/associatedetails';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { ExportExcelService } from '../../eventDataService/export-excel.service';

@Component({
  selector: 'app-associates-summary',
  templateUrl: './associates-summary.component.html',
  styleUrls: ['./associates-summary.component.css']
})
export class AssociatesSummaryComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  lstAssociateDetails:AssociateDetails;
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private excelService:ExportExcelService)
  {
    this._freeApiService.getAssociateDetails().subscribe(
      data=>
      {
        this.lstAssociateDetails=data;
      }
    )
  }

  ngOnInit() {
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Associate Details.xlsx');
  }
}
