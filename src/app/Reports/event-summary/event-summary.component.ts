import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { ViewEventDetails } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { ExportExcelService } from '../../eventDataService/export-excel.service';
import { ViewEventDataService } from '../../eventDataService/view-event-data.service';

@Component({
  selector: 'app-event-summary',
  templateUrl: './event-summary.component.html',
  styleUrls: ['./event-summary.component.css']
})
export class EventSummaryComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  lstEventDetails:ViewEventDetails[];
 // reportFutureEvents:ReportFutureEvents[];
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private excelService:ExportExcelService,private favEvents:ViewEventDataService)
  {
    this._freeApiService.getEventSummary().subscribe(
      data=>
      {
        this.lstEventDetails=data;
        //this.reportFutureEvents=data;
      }
    )
  }

  ngOnInit() {
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Outreach Event Summary.xlsx');
  }
}
