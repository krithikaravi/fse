import { Injectable } from '@angular/core';
import { Observable, of, pipe, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventDetails } from '../classes/eventdetails';
import { EventRegistration } from '../classes/eventregistration';
import { FutureAvailability } from '../classes/futureavailability';
import { AssociateDetails } from '../classes/associatedetails';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { HttpParams } from '@angular/common/http';
import { UserRoles } from '../classes/userroles';
import { Http, Response } from '@angular/http';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { VolunteerDetails } from '../classes/volunteerdetails';
import { ViewEventDetails, BusinessUnit } from '../classes/viewevents';

@Injectable()

export class freeAPIService {

    eventDetails: EventDetails;
    registrationDetails: EventRegistration;
    futureAvailability: FutureAvailability;
    associateDetails: AssociateDetails;
    userRoles: UserRoles;

    constructor(private httpClient: HttpClient) {

        console.log(this.eventDetails, this.registrationDetails);


    }
    uploadBulkVolunteerdDtails(volunteerDetails: VolunteerDetails[]) {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.httpClient.post("http://localhost:49791/api/VolunteerDetails/BulkUploadVolunteerDetails", volunteerDetails, { headers: headers });
    }
    postVolunteerDetails(volunteerDetails?: VolunteerDetails) {
        return this.httpClient.post("http://localhost:49791/api/VolunteerDetails/SaveVolunteerDetails", volunteerDetails);
    }
    getTopLocationEventList(eventDate: string):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/topLocationEventDetailsbyDate/" + eventDate);
    }
    getTopProjectTeamList(eventDate: string):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventRegistrations/topLocationProjectTeambyDate/" + eventDate);
    }

    getEventListBasedOnPocAndDate(pocId: string, eventDate: string):
        Observable<any> {
        let params: any = {
            'emailId': pocId,
            'eventDate': eventDate,
        };
        return this.httpClient.get("http://localhost:49791/api/EventDetails/eventDetailsByPocAndDate/", { params: params });
    }
    getEventListDetails(eventDate: string):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/eventdetailsbydate/" + eventDate);
    }
    getVolunteerHours(employeeId:string):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/volunteerHrsBy/"+ employeeId);
    }
    getAssociateListDetails(eventId):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/associateDetailsByEvent/" + eventId);
    }

    getVolunteerDetails(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/VolunteerDetails");
    }
    getEventRegistrations(id: number): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventRegistrations/" + id);
    }
    getEventRegistration(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventRegistrations");
    }
    postSingleEvent(eventDetails: EventDetails) {
        return this.httpClient.post("http://localhost:49791/api/EventDetails/SaveEventDetails", eventDetails);
    }
    uploadBulkEvents(eventDetails: EventDetails[]) {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.httpClient.post("http://localhost:49791/api/EventDetails/BulkUploadData", eventDetails, { headers: headers });
    }
    updateViewEvent(id: number, eventDetails: ViewEventDetails) {
        return this.httpClient.put("http://localhost:49791/api/EventDetails/UpdateViewEvent/" + id, eventDetails);
    }
    deleteViewEvent(id: number) {
        return this.httpClient.delete("http://localhost:49791/api/EventDetails/DeleteFavEvents/" + id);
    }
    getEventDetails(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails");
    }
    getEventDetailsById(id: number): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/GetEventDetailsById/" + id);
    }
    getEventDetailsByPocId(emailId: string): Observable<any> {
        let params: any = {
            'emailId': emailId

        };
        return this.httpClient.get("http://localhost:49791/api/EventDetails/GetEventDetailsByPocId/", { params: params });
    }
    getUnRegisterEventDetails(): Observable<any> {
        let params: any = {
           // 'associateID': 1,
            'participationId': 2
        };
        return this.httpClient.get("http://localhost:49791/api/EventRegistrations/", { params: params });
    }
    getNotAttendedEventDetails(): Observable<any> {
        // let params: any = {
        //     'associateID': 1,
        //     'participationType': 4,
        //     'participationId': 4
        // };
        return this.httpClient.get("http://localhost:49791/api/EventRegistrations/NotAttended");
    }
    getFavEventDetails(): Observable<any> {
        let params: any = {
            //'associateID': 1,
            'favEvent': true
        };
        //return this.httpClient.get("http://localhost:49791/api/EventDetails/FavEvents/" + 1 + '/' + true);
        return this.httpClient.get("http://localhost:49791/api/EventDetails/FavEvents/" + true);
    }
    postSingleRegistration(eventRegistration: EventRegistration) {
        return this.httpClient.post("http://localhost:49791/api/EventRegistrations/SaveEventRegistrations", eventRegistration);
    }
    postBulkRegistration(eventRegistration: EventRegistration[]) {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.httpClient.post("http://localhost:49791/api/EventRegistrations/BulkUploadRegistration", eventRegistration, { headers: headers });
    }

    putEventStatus(id: number, eventDetails: EventDetails) {
        return this.httpClient.put("http://localhost:49791/api/EventDetails/UpdateEventDetails/" + id, eventDetails);
    }
    putRemoveFavourite(id: number, eventDetails: EventDetails) {
        return this.httpClient.put("http://localhost:49791/api/EventDetails/UpdateEventDetails/" + id, eventDetails);
    }
    postFutureAvailability(futureAvailability: FutureAvailability) {
        return this.httpClient.post("http://localhost:49791/api/FutureAvailability/SaveFutureAvailability", futureAvailability);
    }
    postAssociateDetails(associatedetails: AssociateDetails) {
        return this.httpClient.post("http://localhost:49791/api/AssociateDetails/SaveAssociateDetails", associatedetails);
    }
    getRoles(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/Roles");
    }
    getUserRols(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/UserRoles");
    }
    postUserRoles(userRoles: UserRoles) {
        return this.httpClient.post("http://localhost:49791/api/UserRoles", userRoles);
    }
    updateUserRoles(empId: number, userRoles: UserRoles) {
        return this.httpClient.put("http://localhost:49791/api/UserRoles/" + empId, userRoles);
    }
    deleteEmployee(empId: number) {
        return this.httpClient.delete("http://localhost:49791/api/UserRoles/" + empId);
    }
    putUnRegister(id: number, eventRegistration: EventRegistration) {
        return this.httpClient.put("http://localhost:49791/api/EventRegistrations/" + id, eventRegistration);
    }
    getAssociateDetails(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails");
    }
    getAssociateLoginDetails(email: string): Observable<any> {
        // let params: any = {
        //     'emaiId': email,
        //     'password': password
        // }; 
        // return this.httpClient.get("http://localhost:49791/api/AssociateDetails/?email="+email+"&password="+password+"");
        // return this.httpClient.get("http://localhost:49791/api/AssociateDetails/LoginDetails/?email"+email);
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/LoginDetails/email/?email=" + email);
    }
    getAssociateFutureAvailability(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/FutureAvailability");
    }
    getBeneficiaryDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/BeneficiaryDetails");

    }
    getCouncilDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/CouncilDetails");

    }
    getProjectDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/ProjectDetails");

    }
    getCategoryDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/EventCategory");

    }

    getTransportTypeDetails(eventId):
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/GetTransportTypeDetails/" + eventId);

    }
    // getLocationDetails(): 
    // Observable<any> {
    // return this.httpClient.get("http://localhost:49791/api/EventDetails/LocationDetails");

    // }

    getLocationDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/LocationDetails");
    }
    // var client = new RestClient("https://login.salesforce.com/services/oauth2/token");
    //     var request = new RestRequest(Method.POST);
    //     string encodedBody = string.Format("code={0}&grant_type=authorization_code&client_id={1}&client_secret={2}&redirect_uri={3}",
    //         code, Constants.SF.OAuth_ConsumerKey, Constants.SF.OAuth_ConsumerSecret, Constants.SF.OAuth_CallbackUrl);
    //     request.AddParameter("application/x-www-form-urlencoded", encodedBody, ParameterType.RequestBody);
    //     request.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
    //     var response = client.Execute<AuthToken>(request);
    //     return response.Data;


    ValidateUser(user: any): Observable<any> {
        var userData = "username=" + user.emailId + "&password=" + user.password + "&grant_type=password";
        // var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded','No-Auth':'True' });
        var reqHeader = new HttpHeaders({ 'content': "application/json", 'Content-Type': 'application/x-www-form-urlencoded' });
        //var reqHeader = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
        //reqHeader.append('Access-Control-Allow-Origin', 'Content-Type');

        reqHeader.append('Access-Control-Allow-Credentials', 'true');
        reqHeader.append('Access-Control-Allow-Origin', '*');
        reqHeader.append('Access-Control-Allow-Methods', 'GET');
        reqHeader.append('Access-Control-Allow-Headers', 'application/json');
        // headers:{
        //     'content':"application/json",
        //     'content-type':"application/x-www-form-urlencoded"
        //   }
        //reqHeader.append('Access-Control-Allow-Origin', 'http://localhost:49791/');
        return this.httpClient.post("http://localhost:49791/token", userData, { headers: reqHeader })
            .pipe(
                map(user => { user }),
                catchError(this.errorHandler));

        //  .pipe(map(user => {
        //     // login successful if there's a jwt token in the response
        //     if (user && user.token) {
        //         // store user details and jwt token in local storage to keep user logged in between page refreshes
        //         localStorage.setItem('currentUser', JSON.stringify(user));
        //         this.currentUserSubject.next(user);
        //     }

        //     return user;
    }

    getUserRoles(email: string): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/UserRoles/" + email);
    }

    public isAuthenticated(): boolean {
        return this.getToken() !== null;
    }
    storeToken(token: string) {
        localStorage.setItem("currentUser", token);
    }
    removeToken() {
        localStorage.removeItem("currentUser");
    }
    getToken() {
        return localStorage.getItem("currentUser");
    }
    storeCurrentUserId(token: string) {
        localStorage.setItem("currentUserId", token);
    }
    getCurrentUserId() {
        return localStorage.getItem("currentUserId");
    }
    removeCurrentUserId() {
        localStorage.removeItem("currentUserId");
    }
    errorHandler(error: Response) {
        console.log(error);
        return throwError(error);
    }
    getBusinessUnitDetails(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/AssociateDetails/GetBusinessUnitDetails");
    }

    // getAssociateRoleDetails(email:string): Observable<any> {
    //     let params: any = {
    //         'emaiId': email
    //     };
    //     return this.httpClient.get("http://localhost:49791/api/AssociateDetails/?email="+email+"");
    // }
    getEventInformation(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/GetEventInformation");
    }
    getEventSummary(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/GetEventSummary");
    }

    getChartDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/getChartData");

    }
    getPieChartDetails():
        Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/getPieChartData");

    }
    getPieChartDetailsForPoc(pocId: string):
        Observable<any> {
        let params: any = {
            'emailId': pocId

        };
        return this.httpClient.get("http://localhost:49791/api/EventDetails/getPieChartDataForPoc", { params: params });

    }
    getBuList(): Observable<any> {
        return this.httpClient.get("http://localhost:49791/api/EventDetails/GetBuList");
    }
    notifyBuList(businessUnit: BusinessUnit[]) {

        return this.httpClient.post("http://localhost:49791/api/EventDetails/notifyupcomingeventdetails", businessUnit);
    }

}



