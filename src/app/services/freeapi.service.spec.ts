import { freeAPIService } from './freeapi.service';
import { EventDetails } from '../classes/eventdetails';
import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe(`FakeHttpClientResponses`, () => {

  beforeEach(() => {
    // 0. set up the test environment
    TestBed.configureTestingModule({
      imports: [
        // no more boilerplate code w/ custom providers needed :-)
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it(`should issue a request for getting event details`,
    // 1. declare as async test since the HttpClient works with Observables
    async(
      // 2. inject HttpClient and HttpTestingController into the test
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        // 3. send a simple request
        http.get('http://localhost:49791/api/EventDetails').subscribe();

        // 4. HttpTestingController supersedes `MockBackend` from the "old" Http package
        // here two, it's significantly less boilerplate code needed to verify an expected request
        backend.expectOne({
          url: 'http://localhost:49791/api/EventDetails',
          method: 'GET'
        });
      })
    )
  );

  it(`should issue a request for getting event registrations`,
    // 1. declare as async test since the HttpClient works with Observables
    async(
      // 2. inject HttpClient and HttpTestingController into the test
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        // 3. send a simple request
        http.get('http://localhost:49791/api/EventRegistrations').subscribe();

        // 4. HttpTestingController supersedes `MockBackend` from the "old" Http package
        // here two, it's significantly less boilerplate code needed to verify an expected request
        backend.expectOne({
          url: 'http://localhost:49791/api/EventRegistrations',
          method: 'GET'
        });
      })
    )
  );

  it(`should issue a request for getting volunteer details`,
    // 1. declare as async test since the HttpClient works with Observables
    async(
      // 2. inject HttpClient and HttpTestingController into the test
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        // 3. send a simple request
        http.get('http://localhost:49791/api/VolunteerDetails').subscribe();

        // 4. HttpTestingController supersedes `MockBackend` from the "old" Http package
        // here two, it's significantly less boilerplate code needed to verify an expected request
        backend.expectOne({
          url: 'http://localhost:49791/api/VolunteerDetails',
          method: 'GET'
        });
      })
    )
  );

  it(`should issue a request for getting future availabilities`,
    // 1. declare as async test since the HttpClient works with Observables
    async(
      // 2. inject HttpClient and HttpTestingController into the test
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        // 3. send a simple request
        http.get('http://localhost:49791/api/FutureAvailability').subscribe();

        // 4. HttpTestingController supersedes `MockBackend` from the "old" Http package
        // here two, it's significantly less boilerplate code needed to verify an expected request
        backend.expectOne({
          url: 'http://localhost:49791/api/FutureAvailability',
          method: 'GET'
        });
      })
    )
  );

  it(`should fail when verifying an un-matched request`, async(inject([HttpClient, HttpTestingController],
    (http: HttpClient, backend: HttpTestingController) => {
      http.get('http://localhost:49791/api/FutureAvailability').subscribe();

      backend.match('http://localhost:49791/api/');
      backend.verify();
  })));

  it(`should fail when not sending an expected request`, async(inject([HttpClient, HttpTestingController],
    (http: HttpClient, backend: HttpTestingController) => {
      http.get('http://localhost:49791/api/FutureAvailability').subscribe();

      backend.expectOne('http://localhost:49791/api/');
  })));

  it(`should fail when sending an non-expected request`, async(inject([HttpClient, HttpTestingController],
    (http: HttpClient, backend: HttpTestingController) => {
      http.get('http://localhost:49791/api/FutureAvailability').subscribe();

      backend.expectNone('http://localhost:49791/api/FutureAvailability');
  })));

});

// fdescribe(`FakeHttpClientResponses`, () => {
//   let service: freeAPIService;
// let eventDetailas:EventDetails;
//   beforeEach(() => {
//     service = new freeAPIService();
//   });

// //   it("should create single event", () => {
// //     const qouteText = "This is my first post";
// //     //service.addNewQuote(qouteText);
// //     //expect(service.quoteList.length).toBeGreaterThanOrEqual(1);
// //     service.postSingleEvent(eventDetailas);

// //   });

// //   it("should get view events", () => {
// //    // const qouteText = "This is my first post";
// //     //service.addNewQuote(qouteText);
// //     //expect(service.quoteList.length).toBeGreaterThanOrEqual(1);
// //     service.getEventDetails();
// //   });

// // describe(`FakeHttpClientResponses`, () => {

//     it(`should respond with fake data`, async(inject([HttpClient, HttpTestingController],
//       (http: HttpClient, backend: HttpTestingController) => {
//         http.get('/foo/bar').subscribe((next) => {
//           expect(next).toEqual({ baz: '123' });
//         });
  
//         backend.match({
//           url: '/foo/bar',
//           method: 'GET'
//         })[0].flush({ baz: '123' });
//     })));
  
//   //});

// //   it("should remove a created post from the array of posts", () => {
// //     service.addNewQuote("This is my first post");
// //     service.removeQuote(0);
// //     expect(service.quoteList.length).toBeLessThan(1);
// //   });
// });