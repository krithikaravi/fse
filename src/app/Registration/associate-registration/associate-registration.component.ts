import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewEventDetails } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { NgForm } from '@angular/forms';
import{Location} from'@angular/common';
import { BusinessUnit } from '../../classes/businessUnit';
@Component({
  selector: 'app-associate-registration',
  templateUrl: './associate-registration.component.html',
  styleUrls: ['./associate-registration.component.css']
})
export class AssociateRegistrationComponent implements OnInit {
  detail: any;
  lstEventDetails: ViewEventDetails;
  transportTypeId: number;
  id: number;
  businessUnits: Array<BusinessUnit>;
  showMsg: boolean = false;

  constructor(private loginComponent: LoginComponent, private router: Router, private activatedRoute: ActivatedRoute, private _freeApiService: freeAPIService, private location: Location) {
    let eventId = this.router.url.substring(10);
    this.id = +eventId;
    //let id=1;
    this._freeApiService.getEventDetailsById(this.id).subscribe(
      // data=>
      // {
      //   this.lstEventDetails=data;
      // }
      data => {
        this.lstEventDetails = data;
        console.log(this.lstEventDetails);
      },
      err => {
        window.alert(err.error.Message)
      }
    )
    this._freeApiService.getBusinessUnitDetails()
      .subscribe(businessUnit => {
        this.businessUnits = businessUnit
      });
    this._freeApiService.getTransportTypeDetails(eventId)
      .subscribe(transportType => {
        if (transportType === 'Floating') {
          this.transportTypeId = 2
        }
      });


  }

  goBack(): void {
    this.location.back();
  }
  newData(val: any) {
    //this.lstEventDetails.beneficiaryName = val.beneficiaryName;
    this.lstEventDetails.employeeID = val.employeeID;
    this.lstEventDetails.employeeName = val.employeeName;
    this.lstEventDetails.contactNumber = val.contactNumber;
    this.lstEventDetails.email = val.email;
    this.lstEventDetails.businessUnit = val.businessUnit;
    this._freeApiService.registrationDetails.businessUnit = {
      BusinessUnitName : val ,Id:1
    } as BusinessUnit;
  }

  onBusinessUnitChange(e: any){
    const name: string = e.target.value;
    this._freeApiService.registrationDetails.businessUnit = {
      BusinessUnitName : name,Id:1
    } as BusinessUnit;
  }

  ngOnInit() {

  }

  resetForm(form?:NgForm)
  {
    if(form!=null)
    {
    form.resetForm();
    this._freeApiService.registrationDetails= {
      businessUnit:null,
      businessUnitId:null,
     contactNumber:null,
     email:'',
     employeeId:null,
     employeeName:'',
     endTime:null,
     eventDate:null,
     eventDescription:'',
     eventId:null,
     eventName:'',
    //  livesImpacted:null,
     startTime:null,
     location:'',
     participationId:null,
     //travelHours:null,
     //volunteerHours:null,
     waitingList:false,
     associateDetailsId:null
    }
    }
  }

  onSubmit(form: NgForm) {
    const businessUnit = this.businessUnits.filter(c=>c.BusinessUnitName==form.controls.businessUnit.value)[0];
    form.controls.businessUnit.setValue(businessUnit);
    this._freeApiService.postSingleRegistration(form.value).subscribe(res => {
    });
    this.showMsg = true;
    form.resetForm();
    this.goBack();
  }
}
