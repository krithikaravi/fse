import { TestBed } from '@angular/core/testing';

import { ViewEventDataService } from './view-event-data.service';

describe('ViewEventDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewEventDataService = TestBed.get(ViewEventDataService);
    expect(service).toBeTruthy();
  });
});
