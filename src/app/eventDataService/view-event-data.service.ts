import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViewEventDataService {
  favEvents:any[]=[];
  events: any[] = [
    {
      IsFavourite:false,
      EventID:"EVNT00001",
      BaseLocation:"Chennai",
      BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
      VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    },
    {
      IsFavourite:false,
      
      EventID:"EVNT00002",
      BaseLocation:"Chennai",
       BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
     VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    },
    {
      IsFavourite:false,     
      EventID:"EVNT00003",
      BaseLocation:"Chennai",
       BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
     VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    },
    {
      IsFavourite:false,  
      EventID:"EVNT00004",
      BaseLocation:"Chennai",
       BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
     VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    },
    {
      IsFavourite:false,
      EventID:"EVNT00005",
      BaseLocation:"Chennai",
       BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
     VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    },
    {
      IsFavourite:false,
      EventID:"EVNT00006",
      BaseLocation:"Chennai",
       BeneficiaryName:"KVN Hospital",
      CouncilName:"Medical Needs",
      EventName:"Blood Camp",
      EventDescription:"Donate Bload..Save lives",
      EventDate:"20-02-19",
      EmployeeID:"700000",
      EmployeeName:"Carol",
     VolunteerHours:"5",
      TravelHours:"1",
      LivesImpacted:"200",
      BusinessUnit:[{name:"Insurance"},{name:"Capital life"},{name:"StandaredChartered"},{name:"American Airlines"}],
      Status:"Approved",
     IIEPCategory:""
    }
  ];
  constructor(private http : HttpClient){
  }

  getEvents(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.events);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }
  getFavEvents(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.favEvents);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }
  addFavEvent(event):Observable<any[]>{
    
    this.favEvents.push(event);
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.favEvents);
      }, 200);
    });

  }
  removeFavEvent(event):Observable<any[]>{
   this.favEvents=this.favEvents.filter(i=>i.EventID!=event.EventID);
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.favEvents);
      }, 200);
    });
  }
 
}
