import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociateDetailsComponent } from './associate-details.component';
import { freeAPIService } from '../../services/freeapi.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HomeComponent } from '../../home/home.component';
import { HttpModule } from '@angular/http';

  // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
describe('AssociateDetailsComponent', () => {
  let component: AssociateDetailsComponent;
  let fixture: ComponentFixture<AssociateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociateDetailsComponent,HomeComponent,HeaderComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
        HttpClientTestingModule,HttpModule,FormsModule],
      providers: [{
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
