import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { freeAPIService } from '../../../../services/freeapi.service';
import { Router } from '@angular/router';
import { ViewEventDetails } from '../../../../classes/viewevents';
import { Location } from '@angular/common';
@Component({
  selector: 'app-create-fav-events',
  templateUrl: './create-fav-events.component.html',
  styleUrls: ['./create-fav-events.component.css']
})
export class CreateFavEventsComponent implements OnInit {
  showMsg: boolean = false;
  id: number;
  lstEventDetails: ViewEventDetails;
  transportTypeId: number;
  constructor(private _freeApiService: freeAPIService, private router: Router, private location: Location) {
    // this.resetForm();
    let eventId = this.router.url.substring(17);
    this.id = +eventId;
    //let id=1;
    this._freeApiService.getEventDetailsById(this.id).subscribe(
      data => {
        this.lstEventDetails = data;
      }
    )
    this._freeApiService.getTransportTypeDetails(eventId)
    .subscribe(transportType => {
      if (transportType === 'Floating') {
        this.transportTypeId = 2
      }
    });

  }

  newData(val: ViewEventDetails) {
    //this.lstEventDetails.beneficiaryName = val.beneficiaryName;
    this.lstEventDetails.eventDate = val.eventDate;
    this.lstEventDetails.startTime = val.startTime;
    this.lstEventDetails.EndTime = val.EndTime;
    this.lstEventDetails.email = val.email;
    this.lstEventDetails.businessUnit = val.businessUnit;
    this.lstEventDetails.locationName = val.locationName;
    this.lstEventDetails.Address = val.Address;
    this.lstEventDetails.VolunteersRequired = val.VolunteersRequired;
    this.lstEventDetails.transportTypeId = val.transportTypeId;
    this.lstEventDetails.BoardinPoint = val.BoardinPoint;
    this.lstEventDetails.DropPoint = val.DropPoint;
    this.lstEventDetails.FavEvent = val.FavEvent;
  }

  ngOnInit() {
    //this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
      this._freeApiService.eventDetails = {
        countLocation:null,
        beneficiaryName: '',
        boardingPoint: '',
        councilName: '',
        dropPoint: '',
        endTime: null,
        eventCategory: null,
        eventDate: null,
        eventId: null,
        eventName: '',
        eventStatus: '',
        eventDescription: '',
        favEvent: null,
        locationName: '',
        pocId: null,
        projectName: '',
        startTime: null,
        transportTypeId: null,
        volunteersRequired: null,
        eventCode: null,
        address: '',
      isApproved:null,
      isRejected:null
      //transportType:''
      }
    }
  }


  goBack(): void {
    this.location.back();
  }
  onSubmit(form: NgForm) {
    this._freeApiService.postSingleEvent(form.value).subscribe(res => {
    });
    this.showMsg = true;
    this.resetForm(form);
    this.goBack();
  }
}
