import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEventComponent } from './single-event.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../../services/freeapi.service';
import { HomeComponent } from '../../../home/home.component';
import { HeaderComponent } from '../../../shared/header/header.component';
import { LoginComponent } from '../../../login/login.component';

 // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
describe('SingleEventComponent', () => {
  let component: SingleEventComponent;
  let fixture: ComponentFixture<SingleEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ SingleEventComponent ]
      declarations: [ SingleEventComponent,HomeComponent,HeaderComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
        HttpClientTestingModule,HttpModule,FormsModule],
      providers: [LoginComponent,{
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
