import { Component, OnInit } from '@angular/core';
import { freeAPIService } from '../../../services/freeapi.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.css']
})
export class SingleEventComponent implements OnInit {
  showMsg: boolean = false;
  beneficiaryDetails: Array<String>;
  councilName: Array<string>;
  projectName: Array<String>;
  eventCategory: Array<String>;
  locationName: Array<String>;
  constructor(private _freeApiService:freeAPIService)
  {
    this.resetForm();
    this._freeApiService.getBeneficiaryDetails()
    .subscribe(beneficiaryDetails => 
      { 
        this.beneficiaryDetails = beneficiaryDetails 
      });

      this._freeApiService.getCouncilDetails()
      .subscribe(councilName => 
        { 
          this.councilName = councilName 
       });
      this._freeApiService.getProjectDetails()
        .subscribe(projectName => 
          { 
            this.projectName = projectName 
          });
this._freeApiService.getCategoryDetails()
        .subscribe(eventCategory => 
          { 
            this.eventCategory = eventCategory 
          });
this._freeApiService.getLocationDetails()
          .subscribe(locationName => 
            { 
              this.locationName = locationName 
            });
  
  }
  

  ngOnInit() {
    //this.resetForm();
  }

  resetForm(form?:NgForm)
  {
    if(form!=null)
    {
    form.resetForm();
    this._freeApiService.eventDetails= {
      countLocation:null,
      beneficiaryName:'',
      boardingPoint:'',
      councilName:'',
      dropPoint:'',
      endTime:null,
      eventCategory:null,
      eventDate:null,
      eventId:null,
      eventName:'',
      eventStatus: '',
      eventDescription:'',
      favEvent:null,
      locationName:'',
      pocId:null,
      projectName:'',
      startTime:null,
      transportTypeId:null,
      volunteersRequired:null,
      eventCode:null,
      address:'',
      isApproved:null,
      isRejected:null
      //transportType:''
    }
    }
  }

// //   onSubmit(form:NgForm){
// // this.insertRecord(form);
// //   }
// //   insertRecord(form:NgForm){
// // this._freeApiService.postSingleEvent(form.value).subscribe(res=>{
// //   this.resetForm(form); 
// // });
// //   }


  onSubmit(form:NgForm){
 this._freeApiService.postSingleEvent(form.value).subscribe(res=>{
});
this.showMsg= true;
this.resetForm(form);
  }
//   insertRecord(form:NgForm){
// this._freeApiService.postSingleEvent(form.value).subscribe(res=>{
//   this.resetForm(form);
// });
//   }

}
