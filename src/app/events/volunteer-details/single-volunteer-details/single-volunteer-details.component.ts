import { Component, OnInit } from '@angular/core';

import { freeAPIService } from '../../../services/freeapi.service';
import { VolunteerDetails } from 'src/app/classes/volunteerdetails';
import { AssociateDetails } from '../../../classes/associatedetails';
import { NgForm } from '@angular/forms';
import { EventDetails } from '../../../classes/eventdetails';
@Component({
  selector: 'app-single-volunteer-details',
  templateUrl: './single-volunteer-details.component.html',
  styleUrls: ['./single-volunteer-details.component.css']
})
export class SingleVolunteerDetailsComponent implements OnInit {
  eventList: Array<VolunteerDetails>;
  eventDate: string;
  volunteerDetail: VolunteerDetails;
  employeeID: Array<AssociateDetails>;
  showMsg: boolean = false;
  volunteerHrs:number;
  constructor(private _freeApiService: freeAPIService) {
    this.resetForm();
  }

  ngOnInit() {
  }
  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
      // this._freeApiService.volunteerDetails= {  
      //  eventId:'',
      //  associateDetailsId:'',
      //  volunteerHours:'',
      //  travelHours:'',
      //  livesImpacted:'',
      //  employeeName:'',
      //  eventName:'',
      //  employeeID:'',
      //  participationID:''
      // }
    }
  }
  getEventDate(event: any) {
    this.eventDate = event.target.value;
  }

  onEventSelect(event: any) {
    //const eventId = this.eventList && this.eventList.find(c=>c.eventName === event.target.value).eventId;   
    var eventDetails = this.eventList && this.eventList.find(c => c.eventId == event.target.value);
    const eventId = (eventDetails != null) ? eventDetails.eventId : 0;

    this._freeApiService.getAssociateListDetails(eventId).subscribe(employeeID => {

      this.employeeID = employeeID;
    });
  }

  // onVolunteerChange(event: any) {
  //   var eventDetails = this.eventList && this.eventList.find(c => c.employeeID == event.target.value);
  //   const eventId = (eventDetails != null) ? eventDetails.associateDetailsId : 0;
  // }
onVolunteerChange(event: any) {
    var eventDetails = this.eventList && this.eventList.find(c => c.employeeID == event.target.value);
    const eventId = (eventDetails != null) ? eventDetails.employeeID : 0;
  }
  

  getEventlistbasedonDate() {
    this._freeApiService.getEventListDetails(this.eventDate)
      .subscribe(eventList => {
        this.eventList = eventList;
      });
  }
   getVolunteerHoursbasedonEmpId(employeeID:string){
    this._freeApiService.getVolunteerHours(employeeID)
    .subscribe(volunteerHrs=>{
      this.volunteerHrs=volunteerHrs;
    })
  }
  onSubmit(form: NgForm) {
    this._freeApiService.postVolunteerDetails(form.value).subscribe(res => {
    });
    this.showMsg = true;
    this.resetForm(form);
  }

}
