import { Component, OnInit } from '@angular/core';
import { freeAPIService } from '../../services/freeapi.service';
import { UserRoles,Roles } from '../../classes/userRoles';
import {AssociateDetails} from '../../classes/associatedetails';
import {TemplateRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-configure-roles',
  templateUrl: './configure-roles.component.html',
  styleUrls: ['./configure-roles.component.css']
})
export class ConfigureRolesComponent implements OnInit {
  @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
    @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
    lstUserRoles:Array<UserRoles>;
    userRole:UserRoles;
    editUserRole:UserRoles;
    isNewRecord: boolean;
    statusMessage: string;
    message:string;
    roleDropdownList: Array<Roles>;
    selectedRoles:Roles;
    //dropdownSettings = {};
    isExist:boolean;
    constructor(private _freeApiService:freeAPIService) { 
      this.lstUserRoles=new Array<UserRoles>();
      this.roleDropdownList=new Array<Roles>();
      //this.selectedRoles=new Array<Roles>();
      this.message="Grant Role";
      this.loadUserRoles();
     
    }
    ngOnInit() {
      
      //this.loadUserRoles();
      this.loadRoles();    
      this.selectedRoles=this.roleDropdownList[0];
      // this.dropdownSettings = { 
      //   singleSelection: false, 
      //   text:"Select Roles",
      //   labelKey:"roleType",
      //   primaryKey:"roleId",
      //   selectAllText:'Select All',
      //   unSelectAllText:'UnSelect All',
      //   enableSearchFilter: false,
      //   classes:"myclass custom-class"
      // };  
  }
  loadRoles(){
    //this._freeApiService.getRoles().subscribe(
      this._freeApiService.getRoles().subscribe(
      data=>
         {
           this.roleDropdownList=data;
         });
    
    console.log(this.roleDropdownList);
  }

  loadUserRoles(){
    this._freeApiService.getAssociateDetails().subscribe(
         data=>
            {
              this.lstUserRoles=data;
            });   
                            
  }
  roleChange(args:any){
    this.editUserRole.roleId = args.target.value; 
    this.editUserRole.roleType=args.target.options[args.target.selectedIndex].text; 
  //this.countryName = args.target.options[args.target.selectedIndex].text; 
console.log(args.target.value);
  }
  addUserRole(){
    this.userRole={id:0,employeeID:0,roleId:0,roleType:'',assosiates:null,roles:null};
    this.lstUserRoles.push(this.userRole);
    this.isNewRecord = true;
      }
      editRole(role:UserRoles)
      {
        this.editUserRole=role; 
      }
      loadTemplate(ur: UserRoles) {
        if (this.editUserRole && this.editUserRole.employeeID == ur.employeeID) {
            return this.editTemplate;
        } else {
            return this.readOnlyTemplate;
        }
      }
        saveUserRole() {
          
          if (this.isNewRecord) {
              //add a new UserRole
              this._freeApiService.postUserRoles(this.editUserRole).subscribe(data=> {
                  // this.userRole = resp.json(),
                      this.statusMessage = 'Record Added Successfully.',
                      this.loadUserRoles();
              });
              this.isNewRecord = false;
              this.editUserRole = null;
    
          } else {
              //edit the record
              this._freeApiService.updateUserRoles(this.editUserRole.employeeID, this.editUserRole).subscribe(data=>{
                      this.statusMessage = 'Record Updated Successfully.',
                      this.loadUserRoles();
              });
              this.editUserRole = null;
    
          }
        
      }
         
      cancel() {
        this.editUserRole = null;
    }
    
    //delete userRole
    deleteUserRole(ur: UserRoles) {
        this._freeApiService.deleteEmployee(ur.id).subscribe(data => {
            this.statusMessage = 'Record Deleted Successfully.',
                this.loadUserRoles();
        });
    
    }
  
//   onItemSelect(item:Roles){
    
//     if(this.editUserRole.roles.some(x=>x.roleType==item.roleType))
//     {
//       this.isExist=true;
//     }
//     else{
//       this.isExist=false;
//        this.editUserRole.roles.push(item);
//       //this.selectedRoles.push(item);
     
//     }
//     console.log(this.selectedRoles);
//  }
//  OnItemDeSelect(item:Roles){
//   //this.selectedRoles.slice(item.roleId,1);
//   //console.log(item);
//  }
//  onSelectAll(item: Roles){
//  // this.selectedRoles.push(item);
//  //console.log(item);
//  }
//  onDeSelectAll(item: Roles){
//   //this.selectedRoles.slice(item.roleId,1) ;
//  // console.log(item); 
//  }
  
}
