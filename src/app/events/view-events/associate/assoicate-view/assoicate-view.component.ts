import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
@Component({
  selector: 'app-assoicate-view',
  templateUrl: './assoicate-view.component.html',
  styleUrls: ['./assoicate-view.component.css'],
  providers: [NgbCarouselConfig] 
})
export class AssoicateViewComponent implements OnInit {
  cards = [
    {
      title: 'Planting Trees',
      description: 'Plant tree and save nature',
      buttonText: 'Button',
      img: "assets/events/planting-trees.jpg"
    },
    {
      title: 'Animal Welfare',
      description: 'Save animals',
      buttonText: 'Button',
      img: "assets/events/animal-welfare.jpg"
    },
    {
      title: 'Child Charity',
      description: 'Lets create the bright future',
      buttonText: 'Button',
      img: "assets/events/child.jpg"
    },
    {
      title: 'Blood Camp',
      description: 'Donate and Save Life',
      buttonText: 'Button',
      img: "assets/events/blood-donate.png"
    },
    
  ];
 // cards = [1, 2, 3, 4].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides: any = [[]];
  constructor(config: NgbCarouselConfig,private router: Router) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows=true;
  }
  // chunk(arr, chunkSize) {
  //   let R = [];
  //   for (let i = 0, len = arr.length; i < len; i += chunkSize) {
  //     R.push(arr.slice(i, i + chunkSize));
  //   }
  //   return R;
  // }
  ngOnInit() {
    //this.slides = this.chunk(this.cards, 3);
  }

  onSubmit()
  {
    this.router.navigate(['AssociateRegistration']);
  }
}