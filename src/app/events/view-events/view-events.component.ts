import { Component, ViewChild, TemplateRef, ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { ViewEventDataService } from 'src/app/eventDataService/view-event-data.service';
import { ExportExcelService } from 'src/app/eventDataService/export-excel.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewEventDetails, BusinessUnit } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-view-events',
  templateUrl: './view-events.component.html',
  styleUrls: ['./view-events.component.css']
})
export class ViewEventsComponent implements OnInit {
  @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  isFav = false;
  @ViewChild('table') table: ElementRef;
  lstEventDetails: ViewEventDetails[];
  viewEvents: ViewEventDetails;
  editViewEvents: ViewEventDetails;
  isNewRecord: boolean;
  statusMessage: string;
  isExists: boolean;
  admin: string;
  pmo: string;
  poc: string;
  user: string;
  today = new Date();
  countFav = [];
  dropdownSettings = {};
  dropdownList = [];
  selectedItems: Array<BusinessUnit>;


  constructor(private loginComponent: LoginComponent, private _freeApiService: freeAPIService, private excelService: ExportExcelService, private favEvents: ViewEventDataService) {
    this.lstEventDetails = Array<ViewEventDetails>();
    this.selectedItems = new Array<BusinessUnit>();
    if (_freeApiService.getToken() == "Admin")
      this.admin = "Admin";
    else if (_freeApiService.getToken() == "PMO")
      this.pmo = "PMO";
    else if (_freeApiService.getToken() == "POC")
      this.poc = "POC";
    else if (_freeApiService.getToken() == "User")
      this.user = "User";
  }

  ngOnInit() {
    this.loadEvents();
    this.loadBU();
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select BU Name",
      labelKey: "businessUnitName",
      primaryKey: "id",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "table-active"
    };

  }
  loadBU() {
    this._freeApiService.getBuList().subscribe(
      data => {
        this.dropdownList = data;
      }
    );
  }

  // checkEventDate(event)
  // {
  //  event.eventDate=new Date(event.eventDate);
  //  var diff = new DateDiff(event.eventDate, this.todayDate);
  //  console.log(diff.days());
  //  return diff.days() > 2 ? true:false ;
  // }

  loadEvents() {
    this._freeApiService.getEventDetails().subscribe(
      data => {
        this.lstEventDetails = data;
      }
    );
  }
  editEvent(event: ViewEventDetails) {
    this.editViewEvents = event;
  }
  loadTemplate(event: ViewEventDetails) {
    if (this.editViewEvents && this.editViewEvents.EventCode == event.EventCode) {
      return this.editTemplate;
    } else {
      return this.readOnlyTemplate;
    }
  }
  saveEvent() {
    this._freeApiService.updateViewEvent(this.editViewEvents.EventId, this.editViewEvents).subscribe(data => {
      this.statusMessage = 'Event Updated Successfully.';
      if (this.selectedItems) {
        this._freeApiService.notifyBuList(this.selectedItems).subscribe(data => {
          this.statusMessage = "Notification sent for the selected Bu's";
        });
      }

      this.loadEvents();
    });
    this.editViewEvents = null;

    //}

  }

  cancel() {
    this.editViewEvents = null;
    this.selectedItems = [];
    this.statusMessage = "";

  }

  //delete Event
  deleteEvent(event: ViewEventDetails) {
    this._freeApiService.deleteViewEvent(event.EventId).subscribe(data => {
      this.statusMessage = 'Record Deleted Successfully.',
        this.loadEvents();
    });

  }

  addToFav(event) {
    this._freeApiService.getFavEventDetails().subscribe(data => {
      this.countFav = data;
    });
    if (event.FavEvent && this.countFav.length < 2) {
      event.favouriteEvent = true;
      this.editViewEvents.FavEvent = true;
      this._freeApiService.putEventStatus(event.EventId, event).subscribe(
        data => {

        });
    }
    else {
      event.favouriteEvent = false;
      this.editViewEvents.FavEvent = false;
      this._freeApiService.putEventStatus(event.EventId, event).subscribe(
        data => {
          this.loadEvents();
          if (this.countFav.length >= 2) {
            this.statusMessage = 'Favourite event limit reached.Please delete existing event to add a new event';
          }
        });
    }
  }
  // exportToExcel(){
  //    this.excelService.exportAsExcelFile(this.lstEventDetails,"Volunteering Marathon Event Details");
  // }
  exportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Volunteering Marathon Event Information.xlsx');
  }
}
