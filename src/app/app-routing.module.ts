import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { CreateEventsComponent } from './events/create-events/create-events.component';
import { BulkRegistrationComponent } from './Registration/bulk-registration/bulk-registration.component';
import { AssociateRegistrationComponent } from './Registration/associate-registration/associate-registration.component';
import { EventRegistrationComponent } from './Registration/event-registration/event-registration.component';
import { RegisterComponent } from './Registration/register/register.component';
import { FutureAvailabilityComponent } from './Registration/future-availability/future-availability.component';
import { ViewRegistrationComponent } from './Registration/view-registration/view-registration.component';
import { ViewEventsComponent } from './events/view-events/view-events.component';
import { TopProjectTeamsComponent } from './Reports/top-project-teams/top-project-teams.component';
import { TopLocationsComponent } from './Reports/top-locations/top-locations.component';
import { PostEventTrackingComponent } from './Reports/post-event-tracking/post-event-tracking.component';
import { AssociatesAvailabilityComponent } from './Reports/associates-availability/associates-availability.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {SingleEventComponent} from './events/create-events/single-event/single-event.component';
import{BulkUploadComponent} from './events/create-events/bulk-upload/bulk-upload.component';
import { FavEventsComponent } from './events/create-events/fav-events/fav-events/fav-events.component';
import {AssoicateViewComponent} from './events/view-events/associate/assoicate-view/assoicate-view.component';
import { VolunteerDetailsComponent } from './events/volunteer-details/volunteer-details.component';
import { EventStatusComponent } from './events/event-status/event-status.component';
import { AssociateDetailsComponent } from './events/associate-details/associate-details.component';
import { CreateFavEventsComponent } from './events/create-events/fav-events/create-fav-events/create-fav-events.component';
import { UnRegisteredComponent } from './Reports/un-registered/un-registered.component';
import { FutureEventsComponent } from './Reports/future-events/future-events.component';
import { NotAttendedComponent } from './Reports/not-attended/not-attended.component';
import { EventsInformationComponent } from './Reports/events-information/events-information.component';
import { EventSummaryComponent } from './Reports/event-summary/event-summary.component';
import { AssociatesSummaryComponent } from './Reports/associates-summary/associates-summary.component';
import { ConfigureRolesComponent } from './events/configure-roles/configure-roles.component';
//import { AuthGuardService } from './services/authguardservice';
import { Role } from './classes/roles';
import { UpdateEventComponent } from 'src/app/poc/events/update-event/update-event.component';
import { PostEventActivitiesComponent } from './poc/events/post-event-activities/post-event-activities.component';
import { POCReportsComponent } from './poc/reports/pocreports/pocreports.component';
import { POCDashboardComponent } from './poc/pocdashboard/pocdashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
   {path:'login',component:LoginComponent},
{
  path: 'home',
  children:[
     {
      path:'',
      component:HomeComponent
     } 
  ]

},

{path:'Create',component:CreateEventsComponent},
{path:'BulkRegistration',component:BulkRegistrationComponent},
{path:'AssociateRegistration',component:AssociateRegistrationComponent},
{path:'EventRegistration',component:EventRegistrationComponent},
{path:'Register/:id',component:RegisterComponent},
{path:'FutureAvailability',component:FutureAvailabilityComponent},
// {path:'ViewRegistration',component:ViewRegistrationComponent,canActivate:[AuthGuardService],data: { roles: [Role.User] } },
{path:'ViewRegistration',component:ViewRegistrationComponent},
// {path:'ViewEvents',component:ViewEventsComponent,canActivate:[AuthGuardService]},
// {path:'ViewEvents',component:ViewEventsComponent,canActivate:[AuthGuardService],data: { roles: [Role.Admin] }},
{path:'ViewEvents',component:ViewEventsComponent},
{path:'TopProjectTeams',component:TopProjectTeamsComponent},
{path:'TopLocations',component:TopLocationsComponent},
{path:'PostEventTracking',component:PostEventTrackingComponent},
{path:'AssociatesAvailability',component:AssociatesAvailabilityComponent},
{path:'Dashboard',component:DashboardComponent},
{path:'SingleEvent',component:SingleEventComponent},
{path:'SingleEvent/:id',component:SingleEventComponent},
{path:'BulkUpload',component:BulkUploadComponent},
{path:'FavEvents',component:FavEventsComponent},
{path:'AssoicateView',component:AssoicateViewComponent},
{path:'VolunteerDetails',component:VolunteerDetailsComponent},
{path:'EventStatus',component:EventStatusComponent},
{path:'AssociateDetails',component:AssociateDetailsComponent},
{path:'CreateFavEvents/:id',component:CreateFavEventsComponent},
{path:'UnRegistered',component:UnRegisteredComponent},
{path:'FutureEvents',component:FutureEventsComponent},
{path:'NotAttended',component:NotAttendedComponent},
{path:'EventInformation',component:EventsInformationComponent},
{path:'EventSummary',component:EventSummaryComponent},
{path:'AssociatesSummary',component:AssociatesSummaryComponent},
//,canActivate:[AuthGuardService]
{path:'UpdateEvent',component:UpdateEventComponent},
{path:'PostEvent',component:PostEventActivitiesComponent},
{path:'POCReports',component:POCReportsComponent},
{path:'POCDashboard',component:POCDashboardComponent},


{path:'ConfigureRoles',component:ConfigureRolesComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class AppRoutingModule { 
  
}

