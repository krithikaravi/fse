import { Time } from '@angular/common';
import { BusinessUnit } from './businessUnit';

export class EventRegistration {
    eventId: number;
    businessUnitId: number;
    businessUnit: BusinessUnit;
    associateDetailsId: number;
     //businessUnit:string;
    participationId: number;
    waitingList: boolean;
    employeeName: string;
    employeeId: number;
    contactNumber: number;
    email: string;
    // volunteerHours:number;
    // travelHours:number;
    // livesImpacted:number;
    eventName: String;
    eventDescription: String;
    eventDate: Date;
    startTime: Time;
    endTime: Time;
    location: string;
    
}
export class ViewEventRegistration{
    businessUnit:string;
    topTeamCount:number;
}