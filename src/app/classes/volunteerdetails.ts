import { EventDetails } from '../classes/eventdetails';
export class VolunteerDetails {

    eventId:number;
    associateDetailsId:number;
    volunteerHours:number;
    travelHours:number;
    livesImpacted:number;
    employeeName:string;
    eventName:string;
    employeeID:number;
    participationID:number;    
    event:EventDetails;
    contactNumber: number;
    emailId: string;

    //  eventId:number;    
    //  eventName:string;
    //  event:EventDetails;
    //  volunteerHours:number;
    //  travelHours:number;
    //  livesImpacted:number;
    //  employeeID:number;
}