import {AssociateDetails} from  '../classes/associatedetails';

export class UserRoles
{
    id:number;
    employeeID:number;
    roleId:number;
    roleType:string;
    assosiates:Array<AssociateDetails>;
    roles:Array<Roles>;
}
export class Roles{
    roleId:number;
    roleType:string;
    }
