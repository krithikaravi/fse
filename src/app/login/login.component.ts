import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { freeAPIService } from '../services/freeapi.service';
import { AssociateDetails } from '../classes/associatedetails';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    associateDetails: AssociateDetails[];
    invalid: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private _freeApiService: freeAPIService) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this._freeApiService.getAssociateLoginDetails(this.f.username.value).subscribe(
            data => {
                this.associateDetails = data;
                if (data.length > 0) {
                    this.invalid = false;
                    this._freeApiService.storeToken(data[0].RoleType);
                    this._freeApiService.storeCurrentUserId(data[0].EmailId);
                    this.router.navigate(['/home']);
                }
                else {
                    this.invalid = true;
                }
            })

    }
}
