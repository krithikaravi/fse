import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule, Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CreateEventsComponent } from './events/create-events/create-events.component';
import { BulkRegistrationComponent } from './Registration/bulk-registration/bulk-registration.component';
import { AssociateRegistrationComponent } from './Registration/associate-registration/associate-registration.component';
import { EventRegistrationComponent } from './Registration/event-registration/event-registration.component';
import { RegisterComponent } from './Registration/register/register.component';
import { FutureAvailabilityComponent } from './Registration/future-availability/future-availability.component';
import { ViewRegistrationComponent } from './Registration/view-registration/view-registration.component';
import { ViewEventsComponent } from './events/view-events/view-events.component';
import { TopLocationsComponent } from './Reports/top-locations/top-locations.component';
import { TopProjectTeamsComponent } from './Reports/top-project-teams/top-project-teams.component';
import { PostEventTrackingComponent } from './Reports/post-event-tracking/post-event-tracking.component';
import { AssociatesAvailabilityComponent } from './Reports/associates-availability/associates-availability.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SingleEventComponent } from './events/create-events/single-event/single-event.component';
import { BulkUploadComponent } from './events/create-events/bulk-upload/bulk-upload.component';
import { EventsComponent } from './events/events.component';
import { ViewEventDataService } from './eventDataService/view-event-data.service';
import { ExportExcelService } from './eventDataService/export-excel.service';
import { FavEventsComponent } from './events/create-events/fav-events/fav-events/fav-events.component';
import { AssoicateViewComponent } from './events/view-events/associate/assoicate-view/assoicate-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { freeAPIService } from './services/freeapi.service';
import { VolunteerDetailsComponent } from './events/volunteer-details/volunteer-details.component';
import { SingleVolunteerDetailsComponent } from './events/volunteer-details/single-volunteer-details/single-volunteer-details.component';
import { BulkUploadVolunteerDetailsComponent } from './events/volunteer-details/bulk-upload-volunteer-details/bulk-upload-volunteer-details.component';
import { EventStatusComponent } from './events/event-status/event-status.component';
import { AssociateDetailsComponent } from './events/associate-details/associate-details.component';
import { CreateFavEventsComponent } from './events/create-events/fav-events/create-fav-events/create-fav-events.component';
import { UnRegisteredComponent } from './Reports/un-registered/un-registered.component';
import { FutureEventsComponent } from './Reports/future-events/future-events.component';
import { NotAttendedComponent } from './Reports/not-attended/not-attended.component';
import { EventsInformationComponent } from './Reports/events-information/events-information.component';
import { EventSummaryComponent } from './Reports/event-summary/event-summary.component';
import { AssociatesSummaryComponent } from './Reports/associates-summary/associates-summary.component';
import { SignUpComponent } from './login/sign-up/sign-up.component';
import { ConfigureRolesComponent } from './events/configure-roles/configure-roles.component';
import "reflect-metadata";
//import { ConfigRoleComponent } from './config-role/config-role.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { UpdateEventComponent } from './poc/events/update-event/update-event.component';
import { PostEventActivitiesComponent } from './poc/events/post-event-activities/post-event-activities.component';
import { POCDashboardComponent } from './poc/pocdashboard/pocdashboard.component';
import { POCReportsComponent } from './poc/reports/pocreports/pocreports.component';
import { DatediffPipe } from './events/view-events/datediffPipe';
import { SignInComponent } from './login/sign-in/sign-in.component';
import { PocBulkVolunteerDetailsComponent } from './poc/events/post-event-activities/poc-bulk-volunteer-details/poc-bulk-volunteer-details.component';
import { PocSingleVolunteerDetailsComponent } from './poc/events/post-event-activities/poc-single-volunteer-details/poc-single-volunteer-details.component';
const routes: Routes = [
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  // {path:'**',redirectTo: '/login'},
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    CreateEventsComponent,
    BulkRegistrationComponent,
    AssociateRegistrationComponent,
    EventRegistrationComponent,
    RegisterComponent,
    FutureAvailabilityComponent,
    ViewRegistrationComponent,
    ViewEventsComponent,
    TopLocationsComponent,
    TopProjectTeamsComponent,
    PostEventTrackingComponent,
    AssociatesAvailabilityComponent,
    DashboardComponent,
    SingleEventComponent,
    BulkUploadComponent,
    EventsComponent,
    FavEventsComponent, AssoicateViewComponent, VolunteerDetailsComponent,
    SingleVolunteerDetailsComponent, BulkUploadVolunteerDetailsComponent, EventStatusComponent,
    AssociateDetailsComponent, CreateFavEventsComponent, UnRegisteredComponent, FutureEventsComponent,
     NotAttendedComponent, EventsInformationComponent, EventSummaryComponent, AssociatesSummaryComponent, 
     SignUpComponent,SignInComponent, ConfigureRolesComponent, UpdateEventComponent, PostEventActivitiesComponent, 
     POCDashboardComponent,POCReportsComponent,SingleVolunteerDetailsComponent,DatediffPipe,SingleVolunteerDetailsComponent, PocBulkVolunteerDetailsComponent, PocSingleVolunteerDetailsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    AppRoutingModule,
    NgbModule, HttpClientModule,
    FormsModule,
    ReactiveFormsModule,AngularMultiSelectModule

  ],
  providers: [LoginComponent, 
    ViewEventDataService, 
    ExportExcelService, 
    AssoicateViewComponent, 
    freeAPIService],

    // provider used to create fake backend
    //fakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
