import { Component } from '@angular/core';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FrontEnd';
  showNavBar = false;
  constructor(private loginComponent: LoginComponent) { }
  // showNavBar(){
  //   if(this.loginComponent){
  //     return true;
  //   }
  //   else{
  //     return false;
  //   }
  // }
}


