import { Component,ViewChild,TemplateRef,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import {ViewEventDataService} from 'src/app/eventDataService/view-event-data.service';
import {ExportExcelService} from 'src/app/eventDataService/export-excel.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewEventDetails } from '../../../classes/viewevents';
import { freeAPIService } from '../../../services/freeapi.service';
import { Observable } from 'rxjs';
import {Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {
  @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
    @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  isFav=false;
  @ViewChild('table') table: ElementRef;
  lstEventDetails:ViewEventDetails[];
  viewEvents:ViewEventDetails;
  editViewEvents:ViewEventDetails;
  isNewRecord: boolean;
  statusMessage: string;
  isExists:boolean;
  admin: string;
  pmo: string;
  poc: string;
  user: string;
  todayDate:Date;
  pocId:string
  constructor(private router: Router,private _freeApiService:freeAPIService,private excelService:ExportExcelService,private favEvents:ViewEventDataService)
  {
    this.lstEventDetails= Array<ViewEventDetails>();
     this.todayDate = new Date();
     this.pocId=this._freeApiService.getCurrentUserId();
    
  }

  ngOnInit() {
    
     this.loadEvents();
  }

  // checkEventDate(event)
  // {
  //  event.eventDate=new Date(event.eventDate);
  //  var diff = new DateDiff(event.eventDate, this.todayDate);
  //  console.log(diff.days());
  //  return diff.days() > 2 ? true:false ;
  // }

loadEvents()
{
  this._freeApiService.getEventDetailsByPocId(this.pocId).subscribe(
    data=>
    {
      this.lstEventDetails=data;
    }
  );
}

    editEvent(event:ViewEventDetails)
    {
      this.editViewEvents=event; 
    }
    loadTemplate(event: ViewEventDetails) {
      if (this.editViewEvents && this.editViewEvents.EventId == event.EventId) {
          return this.editTemplate;
      } else {
          return this.readOnlyTemplate;
      }
    }
    saveEvent() {
              this._freeApiService.updateViewEvent(this.editViewEvents.EventId, this.editViewEvents).subscribe(data=>{
                      this.statusMessage = 'Event Updated Successfully.',
                      this.loadEvents();
              });
              this.editViewEvents = null;
    
          //}
        
      }
         
      cancel() {
        this.editViewEvents = null;
    }
    
    //delete Event
    deleteEvent(event: ViewEventDetails) {
        this._freeApiService.deleteViewEvent(event.EventId).subscribe(data => {
            this.statusMessage = 'Event Deleted Successfully.',
                this.loadEvents();
        });
    
    }

  addToFav(event)
  {
//event.FavEvent=!event.FavEvent;
  if(event.FavEvent){
    //this.favEvents.addFavEvent(event);
    event.favouriteEvent = true;
    this._freeApiService.putEventStatus(event.EventId, event).subscribe(
      data => {
      }
    )

  }
  else{
    //this.favEvents.removeFavEvent(event);
    event.favouriteEvent = false;
    this._freeApiService.putEventStatus(event.EventId, event).subscribe(
      data => {
      }
    )
  }
  }
  // exportToExcel(){
  //    this.excelService.exportAsExcelFile(this.lstEventDetails,"Volunteering Marathon Event Details");
  // }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Volunteering Marathon Event Information.xlsx');
  }
}
