import { Component, OnInit } from '@angular/core';
import { VolunteerDetails } from '../../../../classes/volunteerdetails';
import { freeAPIService } from '../../../../services/freeapi.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-poc-bulk-volunteer-details',
  templateUrl: './poc-bulk-volunteer-details.component.html',
  styleUrls: ['./poc-bulk-volunteer-details.component.css']
})
export class PocBulkVolunteerDetailsComponent implements OnInit {

  arrayBuffer: any;
  file: File;
  bulkData: any = [];
  statusMessage:string;
  constructor(private _freeApiService: freeAPIService) { }

  ngOnInit() {
  }
  incomingFile(event) {
    this.file = event.target.files[0];
  }
  OnUpload() {
    let fileReader = new FileReader();

    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary", cellDates: true, dateNF: 'yyyy/mm/dd;@', raw: false });

      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];

      var range = XLSX.utils.decode_range(worksheet['!ref']); // get the range
      for (var R = range.s.r; R <= range.e.r; ++R) {
        for (var C = range.s.c; C <= range.e.c; ++C) {
          var cellref = XLSX.utils.encode_cell({ c: C, r: R }); // construct A1 reference for cell
          if (C == 3 && R != 0) {
            var cell = worksheet[cellref];
            if (cell != undefined) {
              worksheet[cellref].v = worksheet[cellref].w;
            }
          }
          if (C == 4 && R != 0) {
            var cell1 = worksheet[cellref];
            if (cell != undefined) {
              worksheet[cellref].v = worksheet[cellref].w;
            }
          }
        }
      }
      var uploadData = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      this.bulkData = new Array<VolunteerDetails>();
      uploadData.forEach(element => {

        this.bulkData.push(element)
      });
      this._freeApiService.uploadBulkVolunteerdDtails(this.bulkData).subscribe(res => {
        this.statusMessage="File Uploaded Successfully";
      });
    }
    fileReader.readAsArrayBuffer(this.file);
  }


}
